# README #

This project was completed in my first year of university for my Database Systems module. It was written using PostgreSQL and includes both 
DDL and DML statements. The project was designed to interact with a HTML client using Python, which has also been provided.

Adam R. Nickalls
University of East Anglia 2014