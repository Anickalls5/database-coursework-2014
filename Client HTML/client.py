from flask import Flask, render_template, request
from datetime import datetime, timedelta
import psycopg2
import psycopg2.extras

app = Flask(__name__)

file = open('password.txt','r')
password = file.read()
file.close()

#THIS FUNCTION ESTABLISHES A CONNECTION WITH THE DATABASE SERVER
def getConn():
	conn = psycopg2.connect("host='cmpstudb-01.cmp.uea.ac.uk' dbname='pcp14ewu' user='pcp14ewu' password="+password)
	return conn
	
	
	
#THESE FUNCTIONS ARE USED TO RETRIEVE THE TEMPLATES FOR EACH OF THE PAGES	
@app.route('/')
def goToHome():
	return render_template('clientHome.html')
	
@app.route('/customer')
def goToCustomer():
	return render_template('customerCreate.html')
	
@app.route('/staff')
def goToStaff():
	return render_template('staffCreate.html')
	
@app.route('/ticket')
def goToTicket():
	return render_template('ticketCreate.html')

@app.route('/product')
def goToProduct():
	return render_template('productCreate.html')

@app.route('/goToTicketUpdate')
def goToTicketUpdate():
	return render_template('ticketUpdate.html')
	
@app.route('/goToTicketClose')
def goToTicketClose():
	return render_template('ticketClose.html')

@app.route('/goToTicketSearch')
def goToTicketSearch():
	return render_template('ticketSearch.html')

@app.route('/goToCustomerDelete')
def goToCustomerDelete():
	return render_template('customerDelete.html')

	

#THESE FUNCTIONS ARE USED FOR THE CUSTOMERS SECTION OF THE DATABASE
@app.route('/addCustomer',methods=['POST'])
def addCustomer():

	#ESTABLISH THE CONNECTION
	conn = getConn()
	cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
	cur.execute('SET SEARCH_PATH TO coursework_100088688')
	
	#ASSIGN EACH OF THE FORM FIELDS
	customerID = request.form[('id')]
	name = request.form[('name')]
	email = request.form[('email')]
	
	#IF ONE OF THE FIELDS ARE LEFT BLANK, SET THEM EQUAL TO null
	if customerID == '' or customerID == ' ':
		customerID = None
		
	if name == '' or name == ' ':
		name = None
		
	if email == '' or email == ' ':
		email = None
		
	try:
	
		cur.execute('INSERT INTO Customer\
						VALUES(%s,%s,%s)',[customerID,name,email])
		cur.execute('COMMIT')
		
		message = 'The record has been added successfully.'

		cur.execute('SELECT * FROM Customer\
					WHERE CustomerID = '+customerID)
		
		table = cur.fetchall()
		tableHeading = '<tr><td>ID</td><td>Name</td><td>Email</td></tr>'
		error = ''
		
	except Exception as err:
	
		error = err
		table = ''
		tableHeading = ''
		
		if err.pgcode == '23514':
			message = "Please ensure that the customerID is an integer and that the email address contains an '@' character."
		
		elif err.pgcode == '22P02':
			message = 'Please ensure that the ID is present and is a positive integer.'
			
		elif err.pgcode == '23505':
			message = 'That customerID already exists, please use a different one.'
			
		elif err.pgcode == '23502':
			message = 'Please ensure that no fields are left empty.'
			
		elif err.pgcode == '22003':
			message = 'The given number is too large, please insert a smaller integer.'
			
		else:
			message = err.pgcode
			
	finally:
		conn.close()
		return render_template('customerCreate.html',message=message,error=error,table=table,tableHeading=tableHeading)

@app.route('/showCustomer',methods=['GET'])
def showCustomer():

	#ESTABLISH THE CONNECTION
	conn = getConn()
	cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
	cur.execute('SET SEARCH_PATH TO coursework_100088688')
	
	#RETRIEVE THE CUSTOMERS FROM THE APPROPRIATE TABLE
	cur.execute('SELECT * FROM Customer\
				ORDER BY CustomerID')
	
	table = cur.fetchall()
	tableHeading = '<tr><td>CustomerID</td><td>Name</td><td>Email</td></tr>'
	
	#IF NO DATA IS IN THE TABLE, ISSUE A MESSAGE
	if not table:
		message = 'No data found.'
		table = ''
		tableHeading = ''
		
	return render_template('clientHome.html',table=table,tableHeading=tableHeading)
	
@app.route('/deleteCustomer',methods=['GET','POST'])
def deleteCustomer():
	
	#ESTABLISH THE CONNECTION
	conn = getConn()
	cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
	cur.execute('SET SEARCH_PATH TO coursework_100088688')
	
	#ASSIGN THE FORM FIELDS
	customerID = request.form[('id')]
	
	#IF THE CustomerID IS BLANK, SET IT EQUAL TO null
	if customerID == '' or customerID == ' ':
		customerID = None
		
	try:
		
		#CHECK TO SEE IF THE CUSTOMER EXISTS
		cur.execute('SELECT * FROM Customer\
					WHERE CustomerID = %s',[customerID])
		
		table = cur.fetchall()
		
		#RETRIEVE THE TICKETS THAT THE CUSTOMER APPEARS IN, IF APPLICABLE
		cur.execute('SELECT * FROM Ticket\
						WHERE CustomerID = %s',[customerID])
			
		table2 = cur.fetchall()
		table2Heading = '<tr><td>TicketID</td><td>Problem</td><td>Status</td><td>Priority</td><td>LoggedTime</td><td>CustomerID</td><td>ProductID</td></tr>'

		#EXECUTE IF THE CUSTOMER DOES NOT HAVE A RECORD IN THE Customer TABLE
		if not table:		
			message = 'That customer does not exist.'
			error = ''
			table2Heading = ''
			
		else:
			cur.execute('DELETE FROM Customer\
						WHERE CustomerID = %s',[customerID])
					
			cur.execute('COMMIT')
			message = 'The customer has been deleted successfully.'
			error = ''
			table2Heading = ''
	
	except Exception as err:
	
		error = err
		
		#THE TICKET TABLE SHOULD NOT BE OVERWRITTEN FOR THIS PARTICULAR ERROR
		if err.pgcode == '23503':
			message = 'This customer is related to the shown tickets, and cannot be deleted.'
			
		elif err.pgcode == '22P02':
			message = 'Please ensure that the ID is an integer.'
			table2 = ''
			table2Heading = ''
			
		elif err.pgcode == '22003':
			message = 'That ID is too large, please try a smaller one.'
			table2 = ''
			table2Heading = ''
			
		else:
			message = err.pgcode
			table2 = ''
			table2Heading = ''

	finally:
		conn.close()
		return render_template('customerDelete.html',message=message,error=error,table2=table2,table2Heading=table2Heading)

	
	
#THESE FUNCTIONS ARE USED FOR THE STAFF SECTION OF THE DATABASE	
@app.route('/showStaff',methods=['GET'])
def showStaff():

	#ESTABLISH THE CONNECTION
	conn = getConn()
	cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
	cur.execute('SET SEARCH_PATH TO coursework_100088688')
	
	#RETRIEVE ALL DATA FROM THE Staff TABLE
	cur.execute('SELECT * FROM Staff')
	
	table = cur.fetchall()
	tableHeading = '<tr><td>StaffID</td><td>Name</td></tr>'
	
	#IF NO DATA IS IN THE TABLE, ISSUE A MESSAGE
	if not table:
		message = 'No data found.'
		table = ''
		tableHeading = ''
		
	return render_template('clientHome.html',table=table,tableHeading=tableHeading)



#THESE FUNCTIONS ARE USED FOR THE PRODUCT SECTION OF THE DATABASE
@app.route('/showProduct',methods=['GET'])
def showProduct():

	#ESTABLISH THE CONNECTION
	conn = getConn()
	cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
	cur.execute('SET SEARCH_PATH TO coursework_100088688')
	
	#RETRIEVE ALL DATA FROM THE Product TABLE
	cur.execute('SELECT * FROM Product')
	
	table = cur.fetchall()
	tableHeading = '<tr><td>ProductID</td><td>Name</td></tr>'
	
	#IF NO DATA IS IN THE TABLE, ISSUE A MESSAGE
	if not table:
		message = 'No data found.'
		table = ''
		tableHeading = ''
		
	return render_template('clientHome.html',table=table,tableHeading=tableHeading)		



#THESE FUNCTIONS ARE USED FOR THE TICKET SECTION OF THE DATABASE		
@app.route('/showTicket',methods=['GET'])
def showTicket():
	
	#ESTABLISH THE CONNECTION
	conn = getConn()
	cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
	cur.execute('SET SEARCH_PATH TO coursework_100088688')
	
	#RETRIEVE ALL DATA FROM THE Ticket TABLE
	cur.execute("SELECT * FROM Ticket ORDER BY priority, loggedTime")
	
	table = cur.fetchall()
	tableHeading = '<tr><td>TicketID</td><td>Problem</td><td>Status</td><td>Priority</td><td>LoggedTime</td><td>CustomerID</td><td>ProductID</td></tr>'
	title = '<h1>Support Tickets</h1>'
	message = ''
	error = ''
	
	#IF NO DATA IS IN THE TABLE, ISSUE A MESSAGE
	if not table:
		message = 'No data found.'
		table = ''
		tableHeading = ''
		
	return render_template('ticketTable.html',message=message,table=table,tableHeading=tableHeading,title=title)

@app.route('/showOpenTicket',methods=['GET'])
def showOpenTicket():
	
	#ESTABLISH THE CONNECTION
	conn = getConn()
	cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
	cur.execute('SET SEARCH_PATH TO coursework_100088688')
	
	#RETRIEVE ALL DATA FROM THE APPROPRIATE TABLE, SHOWING ALL open TICKETS
	cur.execute('SELECT ticketID, MAX(updateTime) AS latestUpdateTime\
				FROM OpenTickets\
				GROUP BY ticketID\
				ORDER BY ticketID')
	
	table = cur.fetchall()
	tableHeading = '<tr><td>TicketID</td><td>LatestUpdateTime</td></tr>'
	title = '<h1>Open Support Tickets</h1>'
	message = ''
	error = ''
	
	#IF NO DATA IS IN THE TABLE, ISSUE A MESSAGE
	if not table:
		message = 'No data found.'
		table = ''
		tableHeading = ''
		
	return render_template('ticketTable.html',message=message,table=table,tableHeading=tableHeading,title=title)

@app.route('/showClosedTicket',methods=['GET'])
def showClosedTicket():
	
	#ESTABLISH THE CONNECTION
	conn = getConn()
	cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
	cur.execute('SET SEARCH_PATH TO coursework_100088688')
	
	#RETRIEVE ALL DATA FROM THE APPROPRIATE TABLE, SHOWING ALL closed TICKETS
	cur.execute("SELECT Ticket.TicketID, MIN(updateTime)-loggedTime AS intervalBetweenLogAndResponse, MAX(updateTime)-loggedTime AS intervalBetweenLogAndFinalResponse, COUNT(*) AS numberOfUpdates\
				FROM Ticket LEFT JOIN TicketUpdate ON Ticket.TicketID = TicketUpdate.TicketID\
				WHERE status = 'closed'\
				GROUP BY Ticket.ticketID\
				ORDER BY Ticket.ticketID")
	
	table = cur.fetchall()
	tableHeading = '<tr><td>TicketID</td><td>intervalBetweenLogAndResponse</td><td>intveralBetweenLogAndFinalResponse</td><td>numberOfUpdates</td></tr>'
	title = '<h1>Closed Support Tickets</h1>'
	message = ''
	error = ''
	
	#IF NO DATA IS IN THE TABLE, ISSUE A MESSAGE
	if not table:
		message = 'No data found.'
		table = ''
		tableHeading = ''
		
	return render_template('ticketTable.html',message=message,table=table,tableHeading=tableHeading,title=title)
	
@app.route('/addTicket',methods=['POST'])
def addTicket():

	#ESTABLISH THE CONNECTION
	conn = getConn()
	cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
	cur.execute('SET SEARCH_PATH TO coursework_100088688')

	#ASSIGN ALL FORM FIELDS
	ticketID = request.form[('id')]
	problem = request.form[('problem')]
	status = 'open'
	priority = request.form[('priority')]
	loggedTime = datetime.today()
	customerID = request.form[('customerid')]
	productID = request.form[('productid')]
	
	#IF THE FORM FIELDS ARE BLANK, SET THEM EQUAL TO null
	if ticketID == '' or ticketID == ' ':
		ticketID = None
		
	if problem == '' or problem == ' ':
		problem = None
		
	if customerID == '' or customerID == ' ':
		customerID = None
	
	if productID == '' or productID == ' ':
		productID = None
						
	try:
	
		#ADD THE RECORD TO THE Ticket TABLE
		cur.execute('INSERT INTO Ticket\
					VALUES(%s,%s,%s,%s,%s,%s,%s)',[ticketID,problem,status,priority,loggedTime,customerID,productID])
		
		cur.execute('COMMIT')
		
		message = 'The support ticket has been created successfully.'
		
		#RETRIEVE THE NEW RECORD AS CONFIRMATION
		cur.execute('SELECT * FROM Ticket\
					WHERE TicketID = '+ticketID)
		
		table = cur.fetchall()
		tableHeading = '<tr><td>ID</td><td>Problem</td><td>Status</td><td>Priority</td><td>LoggedTime</td><td>CustomerID</td><td>ProductID</td></tr>'
		error = ''
	
	except Exception as err:
	
		#HIDE THE TABLE WHEN AN EXCEPTION IS RAISED
		table = ''
		tableHeading = ''
		error = err
		
		if err.pgcode == '23514':
			message = 'Please ensure that the ID is a positive integer.'
		
		elif err.pgcode == '23502':
			message = 'Please ensure that all fields have been filled.'
			
		elif err.pgcode == '22P02':
			message = 'Please ensure that the ID number is an integer.'
		
		elif err.pgcode == '22003':
			message = 'The ID is too large, please try a smaller number.'
			
		elif err.pgcode == '23503':
			message = 'Please give the ID of an existing product or customer.'
			
		elif err.pgcode == '23505':
			message = 'That ticketID already exists, please try a new one.'
			
		else:
			message = err.pgcode

	finally:
		conn.close()	
		return render_template('ticketCreate.html',message=message,error=error,table=table,tableHeading=tableHeading)
	
@app.route('/ticketSearch',methods=['GET'])
def ticketSearch():

	#ESTABLISH THE CONNECTION
	conn = getConn()
	cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
	cur.execute('SET SEARCH_PATH TO coursework_100088688')

	#ASSIGN FORM FIELDS 
	givenID = request.args[('id')]
	
	#IF THE FIELDS ARE BLANK, SET THEM EQUAL TO null
	if givenID == '' or givenID == ' ':
		message = 'Please give a ticketID number.'
		error = ''
		table = ''
		tableHeading = ''
		givenID = None

	try:			
		
		#RETRIEVE THE APPROPRIATE COLUMNS FOR THE GIVEN TICKET
		cur.execute('SELECT Ticket.TicketID, problem, message, UpdateTime, Staff.Name AS staffName, Customer.Name as CustomerName\
					FROM ((TicketUpdate LEFT JOIN Staff ON TicketUpdate.staffID = Staff.staffID) RIGHT JOIN Ticket ON TicketUpdate.TicketID = Ticket.TicketID) JOIN Customer ON Customer.CustomerID = Ticket.CustomerID\
					WHERE Ticket.TicketID = '+givenID+'\
					ORDER BY UpdateTime')
					
		table = cur.fetchall()
		tableHeading = '<tr><td>TicketID</td><td>OriginalProblem</td><td>UpdateMessage</td><td>UpdateTime</td><td>StaffName</td><td>CustomerName</td></tr>'
		
		#IF THE TICKET EXISTS, DISPLAY THE TABLE
		if table:
			message = ''
			error = ''
			table = table
			tableHeading = tableHeading
			
		else:
			message = 'The ID is not present in the ticket list.'
			error = ''
			table = ''
			tableHeading = ''
	
	except Exception as err:
		
		error = err
		table = ''
		tableHeading = ''
		
		if err.pgcode == '42703':
			message = 'Please give an integer.'
			
		else:
			message = err.pgcode

	
	finally:
		conn.close()
		return render_template('ticketSearch.html',message=message,error=error,table=table,tableHeading=tableHeading)

@app.route('/closeTicket',methods=['POST'])
def closeTicket():
		
	#ESTABLISH THE CONNECTION
	conn = getConn()
	cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
	cur.execute('SET SEARCH_PATH TO coursework_100088688')

	#ASSIGN THE FORM FIELDS
	ticketID = request.form[('id')]
	
	#IF THE FIELDS ARE BLANK, SET THEM EQUAL TO null
	if ticketID == '' or ticketID == ' ':
		ticketID = None
						
	try:
	
		#CHECK IF THE GIVEN ID IS AN EXISTING TICKET
		cur.execute('SELECT * FROM Ticket\
					WHERE TicketID = %s',[ticketID])
		
		table1 = cur.fetchall()
		
		#CHECK IF THE GIVEN ID IS A closed TICKET
		cur.execute("SELECT * FROM Ticket\
					WHERE status = 'closed' AND TicketID = %s",[ticketID])
		
		table2 = cur.fetchall()
		
		#IF THE TICKET DOES NOT EXIST
		if not table1:
			message = 'That ticket does not exist.'
			error = ''
			
		#IF THE TICKET IS closed
		elif table2:
			message = 'That ticket has already been closed.'
			error = ''
			
		else:
			cur.execute("UPDATE Ticket SET status = 'closed'\
						WHERE TicketID = "+ticketID)
		
			cur.execute('COMMIT')
			message = 'The ticket has been closed successfully.'
			error = ''
	
	except Exception as err:
		
		error = err
		
		if err.pgcode == '22P02':
			message = 'Please ensure that the ID is an integer.'
		
		elif err.pgcode == '22003':
			message = 'The given ID is too large, please try a smaller one.'
			
		else:
			message = err.pgcode

	finally:
		conn.close()	
		return render_template('ticketClose.html',message=message,error=error)
		
		
		
#THESE FUNCTIONS ARE USED FOR THE TICKETUPDATE SECTION OF THE DATABASE		
@app.route('/addTicketUpdate',methods=['GET','POST'])
def addTicketUpdate():
	
	#ESTABLISH THE CONNECTION
	conn = getConn()
	cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
	cur.execute('SET SEARCH_PATH TO coursework_100088688')

	#ASSIGN THE FORM FIELDS
	updateID = request.form[('id')]
	updateMessage = request.form[('message')]
	updateTime = datetime.today()
	ticketID = request.form[('ticketid')]
	staffID = request.form[('staffid')]
	
	#IF THE FIELDS ARE BLANK, SET THEM EQUAL TO null
	if updateID == '' or updateID == ' ':
		updateID = None
	
	if updateMessage == '' or updateMessage == ' ':
		updateMessage = None
	
	if ticketID == '' or ticketID == ' ':
		ticketID = None
	
	if staffID == '' or staffID == ' ':
		staffID = None
	
	try:
		#INSERT THE DATA TO SEE IF IT MEETS THE TABLE'S DOMAIN CONSTRAINTS
		cur.execute('INSERT INTO TicketUpdate\
						VALUES(%s,%s,%s,%s,%s)',[updateID,updateMessage,updateTime,ticketID,staffID])
		
		#CHECK IF THE TICKET IS open
		cur.execute("SELECT * FROM Ticket\
						WHERE status = 'open' AND TicketID = %s",[ticketID])
						
		table = cur.fetchall()
		
		#IF THE TICKET IS open, SAVE THE CHANGES TO THE TABLE
		if table:
			cur.execute('COMMIT')
			message = 'The support ticket has been updated successfully.'
			error = ''
		
		#IF NOT, THE CHANGES WILL BE UNDONE
		else:
			cur.execute('ROLLBACK')
			message = 'The support ticket is closed and can no longer be updated.'
			error = ''		
	
	except Exception as err:
		
		error = err
		
		if err.pgcode == '23502':
			message = 'Please ensure that no fields are left empty.'
			
		elif err.pgcode == '23505':
			message = 'That UpdateID already exists, please use a different one.'
			
		elif err.pgcode == '22003':
			message = 'The given UpdateID is too large, please give a smaller number.'
		
		elif err.pgcode == '23503':
			message = 'Please ensure that both the staff member and ticket currently exist.'
			
		else:
			message = err.pgcode
			
	finally:
		conn.close()
		return render_template('ticketUpdate.html',message=message,error=error)
		
@app.route('/closeOldTickets')
def closeOldTickets():

	currentDate = datetime.today()
	
	#ESTABLISH THE CONNECTION
	conn = getConn()
	cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
	cur.execute('SET SEARCH_PATH TO coursework_100088688')
						
	try:
		#RETRIEVE AND UPDATE ALL TICKETS THAT HAVE NOT BEEN UPDATED FOR 24 HOURS
		cur.execute("UPDATE Ticket\
					SET status = 'closed'\
					WHERE TicketID IN (SELECT TicketID\
										FROM TicketUpdate\
										GROUP BY TicketID\
										HAVING MAX(updateTime) + '24 hours' < NOW())")
		
		cur.execute('COMMIT')
		
		message = 'All tickets not updated in the last 24 hours have been closed.'
		error = ''
	
	except Exception as err:
			message = err.pgcode
			error = err
	
	finally:
		conn.close()	
		return render_template('ticketUpdate.html',message=message,error=error)		
		
		
		
#PYTHON CODE		
if __name__ == '__main__':
	app.run(debug = True)