﻿CREATE SCHEMA COURSEWORK_100088688 AUTHORIZATION pcp14ewu;

SET SEARCH_PATH TO COURSEWORK_100088688, PUBLIC;


--DDL STATEMENTS

CREATE TABLE Staff
(
	StaffID		INTEGER CHECK(StaffID > 0),
	Name 		VARCHAR(40) NOT NULL CHECK(Name NOT LIKE ' %' OR Name NOT LIKE ''),

	PRIMARY KEY(StaffID)
);


CREATE TABLE Product
(
	ProductID 	INTEGER CHECK(ProductID > 0),
	Name 		VARCHAR(40) NOT NULL UNIQUE CHECK(Name NOT LIKE ' %'),

	PRIMARY KEY(ProductID)
);


CREATE TABLE Customer
(
	CustomerID 	INTEGER CHECK(CustomerID > 0),
	Name 		VARCHAR(40) NOT NULL CHECK(Name NOT LIKE ' %'),
	Email 		VARCHAR(40) NOT NULL CHECK(Email LIKE '%@%'),

	PRIMARY KEY(CustomerID)
);


CREATE TABLE Ticket
(
	TicketID 	INTEGER CHECK(TicketID > 0),
	Problem 	VARCHAR(1000) NOT NULL CHECK(Problem NOT LIKE ' %'),
	Status 		VARCHAR(20) NOT NULL CHECK(Status IN ('open','closed')),
	Priority 	INTEGER NOT NULL CHECK(Priority IN (1,2,3)),
	LoggedTime 	TIMESTAMP(0) NOT NULL,
	CustomerID 	INTEGER NOT NULL,
	ProductID 	INTEGER NOT NULL,

	PRIMARY KEY(TicketID),
	FOREIGN KEY(CustomerID) REFERENCES Customer ON DELETE RESTRICT,
	FOREIGN KEY(ProductID) REFERENCES Product ON DELETE RESTRICT
);



CREATE TABLE TicketUpdate
(
	TicketUpdateID	INTEGER CHECK(TicketUpdateID > 0),
	Message		VARCHAR(1000) NOT NULL CHECK(Message NOT LIKE '' OR Message NOT LIKE ' %'),
	UpdateTime	TIMESTAMP(0) NOT NULL,
	TicketID	INTEGER NOT NULL,
	StaffID		INTEGER,

	PRIMARY KEY(TicketUpdateID),
	FOREIGN KEY(TicketID) REFERENCES Ticket ON DELETE RESTRICT,
	FOREIGN KEY(StaffID) REFERENCES Staff ON DELETE RESTRICT
);



CREATE VIEW OpenTickets AS
SELECT Ticket.ticketID, Problem, Status, Priority, LoggedTime, CustomerID, ProductID, TicketUpdateID, Message, UpdateTime, StaffID
FROM Ticket LEFT JOIN TicketUpdate ON Ticket.ticketID = TicketUpdate.ticketID
WHERE status = 'open';


CREATE INDEX CustomerID ON Customer (CustomerID);
CREATE INDEX StaffID ON Staff (StaffID);
CREATE INDEX ProductID ON Product (ProductID);
CREATE INDEX TicketID ON Ticket (TicketID);
CREATE INDEX TicketUpdateID ON TicketUpdate (TicketUpdateID);

	

--INSERT DATA STATEMENTS

INSERT INTO Staff VALUES(354,'Buleyburn, Beynard');
INSERT INTO Staff VALUES(298,'Crescent, Abigail');
INSERT INTO Staff VALUES(246,'Foxfayre, Fortnight');
INSERT INTO Staff VALUES(731,'Harvest, Henrietta');
INSERT INTO Staff VALUES(735,'Manton, Callum');
INSERT INTO Staff VALUES(346,'Mint, Merryweather');
INSERT INTO Staff VALUES(452,'Moors, Edward');
INSERT INTO Staff VALUES(600,'Pane, Malcolm');
INSERT INTO Staff VALUES(255,'Richbishop, Glenn');
INSERT INTO Staff VALUES(987,'Richbishop, Mary');
INSERT INTO Staff VALUES(743,'Stirling, David');
INSERT INTO Staff VALUES(865,'Stirling, Kelly');
INSERT INTO Staff VALUES(257,'Thistle, Therron');
INSERT INTO Staff VALUES(414,'Timore, Vervuilen');
INSERT INTO Staff VALUES(753,'Wax, Barrett');


INSERT INTO Product VALUES(1048,'Motherboard');
INSERT INTO Product VALUES(1131,'Fuse (x5)');
INSERT INTO Product VALUES(1205,'Computer Chip (Small)');
INSERT INTO Product VALUES(1434,'Power Cable (3m)');
INSERT INTO Product VALUES(1864,'Tower Case (Red)');
INSERT INTO Product VALUES(1948,'Fuse (x20)');
INSERT INTO Product VALUES(2042,'Tower Case (Blue)');
INSERT INTO Product VALUES(2442,'Monitor');
INSERT INTO Product VALUES(2597,'Computer Chip (Medium)');
INSERT INTO Product VALUES(2857,'Computer Chip (Large)');
INSERT INTO Product VALUES(4950,'Heat Fan');
INSERT INTO Product VALUES(6005,'Power Cable (5m)');
INSERT INTO Product VALUES(6164,'Tower Case (Black)');
INSERT INTO Product VALUES(7406,'Resistor');
INSERT INTO Product VALUES(8353,'Power Cable (7m)');
INSERT INTO Product VALUES(9454,'Fuse (x10)');


INSERT INTO Customer VALUES(235,'Charity, Hannah','h_charity03@gmail.com');
INSERT INTO Customer VALUES(845,'Corwin, Sam','corwin_s4@orangehome.co.uk');
INSERT INTO Customer VALUES(113,'Higsbury, Jessica','jhh9@aol.com');
INSERT INTO Customer VALUES(351,'Leffley, Luke','luke_L08@yahoomail.co.uk');
INSERT INTO Customer VALUES(245,'Manton, Matt','mmanton@gmail.com');
INSERT INTO Customer VALUES(935,'Rivers, Archie','arrivers_w@aol.co.uk');
INSERT INTO Customer VALUES(216,'Richbishop, Megan','megr7@gmail.com');
INSERT INTO Customer VALUES(173,'Spring, Amanda','aspring_2@orangehome.co.uk');
INSERT INTO Customer VALUES(262,'Stirling, Sarah','sstirling6@outlook.co.uk');
INSERT INTO Customer VALUES(863,'Woods, Foster','foswoods5@outlook.co.uk');


INSERT INTO Ticket VALUES(24241,'The product is not working!','open',1,'05/03/15 10:34:57',113,1948);
INSERT INTO Ticket VALUES(24852,'Pieces missing','closed',1,'24/08/14 09:05:30',845,1048);
INSERT INTO Ticket VALUES(24592,'I am unable to fix the product to my PC.','closed',2,'01/04/15 13:20:13',173,2042);
INSERT INTO Ticket VALUES(24695,'Are there any additional accessories to my product?','closed',3,'24/02/14 17:08:03',935,1864);
INSERT INTO Ticket VALUES(24855,'Wrong colour delivered.','open',1,'25/09/13 14:54:32',245,2042);
INSERT INTO Ticket VALUES(24590,'Connectors and adaptors available?','open',3,'04/05/15 10:13:31',935,8353);
INSERT INTO Ticket VALUES(24177,'Self wiring?','open',2,'12/06/14 22:05:32',262,1434);
INSERT INTO Ticket VALUES(24399,'How to install?','open',3,'13/08/11 02:42:45',845,9454);
INSERT INTO Ticket VALUES(24786,'Plastic seal damaged','open',1,'09/01/15 18:00:32',235,8353);
INSERT INTO Ticket VALUES(24134,'Compatibility issues','closed',2,'12/11/13 17:42:11',245,1205);
INSERT INTO Ticket VALUES(24444,'Cleaning help?','closed',3,'04/10/12 15:54:52',173,4950);
INSERT INTO Ticket VALUES(24598,'Cable ties available?','open',3,'25/03/14 13:07:49',216,6005);


INSERT INTO TicketUpdate VALUES(100343,'In the manual of the product is a serial for each of the pieces. Give us a list of missing pieces and we can sort out a replacement right away.','24/08/14 16:25:12',24852,298);
INSERT INTO TicketUpdate VALUES(100463,'To clean the heat fan, use a can of pressurised air to remove the dust from the blades. NEVER apply liquids to the fan, or use a heated air source (i.e. a hairdryer).','05/10/12 10:24:08',24444,731);
INSERT INTO TicketUpdate VALUES(100443,'The computer chip can be applied to the circuits listed in the product manual. If you cannot apply the computer chip, come and see us in store and we will sort this issue out, free of charge.','15/11/13 09:12:45',24134,753);
INSERT INTO TicketUpdate VALUES(100332,'There should be a product manual included with specific instructions, if not, we have a range of downloadable manuals on our website that will give you instructions you require.','02/04/15 14:11:43',24592,346);
INSERT INTO TicketUpdate VALUES(100019,'At this time, we have no additional accessories until the next season, watch this space!','26/02/14 09:47:22',24695,452);
INSERT INTO TicketUpdate VALUES(100426,'Please list your problem in more detail.','05/03/15 12:10:01',24241,257);
INSERT INTO TicketUpdate VALUES(100427,'I have tried placing it in in the plug and it is not working at all.','05/03/15 12:54:23',24241,NULL);
INSERT INTO TicketUpdate VALUES(100428,'Is the appliance the issue? Try changing the position of the fuse.','05/03/15 13:20:03',24241,257);
INSERT INTO TicketUpdate VALUES(100429,'The appliance is new and I have switched the fuse around.','05/03/15 13:46:19',24241,NULL);
INSERT INTO TicketUpdate VALUES(100430,'Occasionally there are fuses which break during transportation. Either try a new plug socket or a different fuse. If you do find broken fuses, we will send replacements.','05/03/15 14:00:12',24241,257);
INSERT INTO TicketUpdate VALUES(100431,'I have replaced it with a different fuse, the other one was the cause of the problem, thank you!','05/03/15 14:16:19',24241,NULL);
INSERT INTO TicketUpdate VALUES(100345,'The missing pieces aure A39, B32 and P32, thanks!','25/08/14 08:56:37',24852,NULL);
INSERT INTO TicketUpdate VALUES(100444,'I have checked the product manual and the circuit is listed there, but I cannot find any instructions for it.','15/11/13 11:03:30',24134,NULL);
INSERT INTO TicketUpdate VALUES(100445,'OK, we include all of our instruction manuals on our homepage for free download. Use your product to find the right one and it should be included, as I have just checked the pdf file. Sorry for any inconvenience  caused.','15/11/13 11:49:17',24134,753);


-- INSERT ERRONEOUS DATA

INSERT INTO Staff VALUES('A03','Cuckoo, Marana');
INSERT INTO Staff VALUES(-204,'Equinox, Edgar');
INSERT INTO Staff VALUES(NULL,'Betts, Elizabeth');
INSERT INTO Staff VALUES(293,NULL);


INSERT INTO Product VALUES(NULL,'Blank Disc');
INSERT INTO Product VALUES(-3402,'Negative Film');
INSERT INTO Product VALUES('A443','Letter Opener');
INSERT INTO Product VALUES(2958,NULL);


INSERT INTO Customer VALUES('43F','Stoane, Michael','mstoane@outlook.co.uk');
INSERT INTO Customer VALUES(-348,'Perman, Shaun','shaun0perman@gmail.com');
INSERT INTO Customer VALUES(NULL,'Cream, Victoria','victoria_cream@gdhs.ac.uk');
INSERT INTO Customer VALUES(244,NULL,'unknown@googlemail.com');
INSERT INTO Customer VALUES(241,'McClieg, John','johnMcClieg.com');


INSERT INTO Ticket VALUES('23S32','The problem is that the product is not working!','closed',1,'03/06/15 12:59:49',216,1434);
INSERT INTO Ticket VALUES(-34424,'The problem is that the product is not working!','closed',1,'03/06/15 12:59:49',216,1434);
INSERT INTO Ticket VALUES(NULL,'The problem is that the product is not working!','closed',1,'03/06/15 12:59:49',216,1434);
INSERT INTO Ticket VALUES(24000,NULL,'closed',1,'03/06/15 12:59:49',216,1434);
INSERT INTO Ticket VALUES(24000,'The problem is that the product is not working!','UNKNOWN',1,'03/06/15 12:59:49',216,1434);
INSERT INTO Ticket VALUES(24000,'The problem is that the product is not working!','closed',4,'03/06/15 12:59:49',216,1434);
INSERT INTO Ticket VALUES(24000,'The problem is that the product is not working!','closed',1,'19th May',216,1434);
INSERT INTO Ticket VALUES(24000,'The problem is that the product is not working!','closed',1,'03/06/15 12:59:49',6379,1434);
INSERT INTO Ticket VALUES(24000,'The problem is that the product is not working!','closed',1,'03/06/15 12:59:49',216,282572);


INSERT INTO TicketUpdate VALUES('A10024','My product is completely wrong!','05/02/13 09:36:12',24852,354);
INSERT INTO TicketUpdate VALUES(-100324,'My product is completely wrong!','05/02/13 09:36:12',24852,354);
INSERT INTO TicketUpdate VALUES(NULL,'My product is completely wrong!','05/02/13 09:36:12',24852,354);
INSERT INTO TicketUpdate VALUES(100000,NULL,'05/02/13 09:36:12',24852,354);
INSERT INTO TicketUpdate VALUES(100000,'My product is completely wrong!','20th May',24852,354);
INSERT INTO TicketUpdate VALUES(100000,'My product is completely wrong!','05/02/13 09:36:12',22,354);
INSERT INTO TicketUpdate VALUES(100000,'My product is completely wrong!','05/02/13 09:36:12',24852,352472);



-- TASKS


--(4)

SELECT Ticket.ticketID, MAX(updateTime) AS latestUpdateTime
FROM Ticket LEFT JOIN TicketUpdate ON Ticket.ticketID = TicketUpdate.ticketID
WHERE status = 'open'
GROUP BY Ticket.ticketID
ORDER BY Ticket.ticketID;


--(6)

SELECT Ticket.TicketID, problem, message, UpdateTime, Staff.Name AS staffName, Customer.Name as CustomerName
FROM ((TicketUpdate LEFT JOIN Staff ON TicketUpdate.staffID = Staff.staffID) RIGHT JOIN Ticket ON TicketUpdate.TicketID = Ticket.TicketID) JOIN Customer ON Customer.CustomerID = Ticket.CustomerID
ORDER BY UpdateTime;


--(7)

SELECT Ticket.TicketID, MIN(updateTime)-loggedTime AS intervalBetweenLogAndResponse, MAX(updateTime)-loggedTime AS intervalBetweenLogAndFinalResponse, COUNT(*) AS numberOfUpdates
FROM Ticket LEFT JOIN TicketUpdate ON Ticket.TicketID = TicketUpdate.TicketID
WHERE status = 'closed'
GROUP BY Ticket.ticketID
ORDER BY Ticket.ticketID;

--(8)

UPDATE Ticket
SET status = 'closed'
WHERE TicketID IN (SELECT TicketID
		   FROM TicketUpdate
		   GROUP BY TicketID
		   HAVING MAX(updateTime) + '24 hours' < NOW())
		   


-- TEST DATA (SYSTEM)

INSERT INTO Staff (StaffID, Name) VALUES (1, 'Bob Mann');
INSERT INTO Staff (StaffID, Name) VALUES (2, 'Sarah Walker');


INSERT INTO Product (ProductID, Name) VALUES (1, 'MyBackup');
INSERT INTO Product (ProductID, Name) VALUES (2, 'MyOrganiser');
INSERT INTO Product (ProductID, Name) VALUES (3, 'MyMediaStore');


INSERT INTO Customer (CustomerID, Name,email) VALUES (1, 'John Smith', 'JohnSmith@nowhere.net');
INSERT INTO Customer (CustomerID, Name,email) VALUES (2, 'Kate Jones', 'Katez@nowhere.net');
INSERT INTO Customer (CustomerID, Name,email) VALUES (3, 'Harvey Black', 'harveyb@nowhere.net');
INSERT INTO Customer (CustomerID, Name,email) VALUES (4, 'Ian Dent', 'ident@nowhere.net');


INSERT INTO Ticket (TicketID, Problem, Status, Priority, LoggedTime, CustomerID, ProductID) VALUES
	(1000, 'Can I install MyBackup on Windows Server 2003?', 'open', 3, '2015-04-14 09:30', 2, 1);
INSERT INTO TicketUpdate (TicketUpdateID, Message, UpdateTime, StaffID, TicketID) VALUES 
	(10000, 'Yes. MyBackup is support on Windows Server 2003 running as an interactive and service application','2015-04-15 14:27', 1, 1000);
UPDATE Ticket SET status = 'closed' WHERE TicketID = 1000;


INSERT INTO Ticket (TicketID, Problem, Status, Priority, LoggedTime, CustomerID, ProductID) VALUES
	(1001, 'The alarm always seems to sound 15 minutes before an event. Can I change this?', 'open', 3, '2015-04-18 17:06', 2, 2);
INSERT INTO TicketUpdate (TicketUpdateID, Message, UpdateTime, TicketID, StaffID) VALUES 
	(10001, 'Right click on the appointment and choose properties, then click the Advanced button. Options to change the alarm time can be found here.',
	'2015-04-18 17:47', 1001, 1);
INSERT INTO TicketUpdate (TicketUpdateID, Message, UpdateTime, TicketID, StaffID) VALUES 
	(10002, 'Thanks, that works fine. Can I modify all future events at once, or must I do each one individually?',
	'2015-04-19 09:08', 1001, NULL);
INSERT INTO TicketUpdate (TicketUpdateID, Message, UpdateTime, TicketID, StaffID) VALUES 
	(10003, 'Yes. Switch to details view then select Edit | Select All then right click and select advanced options.',
	'2015-04-19 10:21', 1001, 1);
UPDATE Ticket SET status = 'closed' WHERE TicketID = 1001;


INSERT INTO Ticket (TicketID, Problem, Status, Priority, LoggedTime, CustomerID, ProductID) VALUES
	(1002, 'I am trying to restore files from my backup but I get the message: index damaged, please rebuild?', 'open', 1, '2015-04-18 10:01', 3, 1);
INSERT INTO TicketUpdate (TicketUpdateID, Message, UpdateTime, TicketID, StaffID) VALUES 
	(10004, 'Use the Tools | Index | Repair option to rebuild the backup.log file, then try again',
	'2015-04-19 09:08', 1002, 2);
INSERT INTO TicketUpdate (TicketUpdateID, Message, UpdateTime, TicketID, StaffID) VALUES 
	(10005, 'That worked fine. Thank you!', '2015-04-19 09:38', 1002, NULL);
UPDATE Ticket SET status = 'closed' WHERE TicketID = 1002;

INSERT INTO Ticket (TicketID, Problem, Status, Priority, LoggedTime, CustomerID, ProductID) VALUES
	(1003, 'Is it possible to colour code my event items?', 'open', 2, '2015-04-19 09:48', 4, 2);
INSERT INTO TicketUpdate (TicketUpdateID, Message, UpdateTime, TicketID, StaffID) VALUES 
	(10006, 'Event items can be categorised (click on Advanced when editing) but always appear in the same colour',
	'2015-04-19 11:51', 1003, 2);
INSERT INTO TicketUpdate (TicketUpdateID, Message, UpdateTime, TicketID, StaffID) VALUES 
	(10007, 'Ok, thanks. I think this would be a useful feature for a later release.',
	'2015-04-19 12:03', 1003, NULL);
INSERT INTO Ticket (TicketID, Problem, Status, Priority, LoggedTime, CustomerID, ProductID) VALUES
	(1004, 'MyBackup is not recognising my DVD-RW drive?', 'open', 2, '2015-04-19 11:10', 1, 1);
INSERT INTO Ticket (TicketID, Problem, Status, Priority, LoggedTime, CustomerID, ProductID) VALUES
	(1005, 'How can I get help on specific items?', 'open', 2, '2015-04-10 14:10', 3, 3);
INSERT INTO TicketUpdate (TicketUpdateID, Message, UpdateTime, TicketID, StaffID) VALUES 
	(10008, 'Point the mouse and press the right key over any area in the screen',
	'2015-04-10 15:03', 1005, 2);


	
-- DROP/ DELETE STATEMENTS

DELETE FROM TicketUpdate;
DELETE FROM Ticket;
DELETE FROM Customer;
DELETE FROM Staff;
DELETE FROM Product;

DROP VIEW OpenTickets;
DROP TABLE TicketUpdate;
DROP TABLE Ticket;
DROP TABLE Customer;
DROP TABLE Staff;
DROP TABLE Product;

DROP INDEX staffID;
DROP INDEX productID;
DROP INDEX customerID;
DROP INDEX ticketID;
DROP INDEX TicketUpdateID;